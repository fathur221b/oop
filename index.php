<?php 
    require_once ('Animal.php');
    require_once ('Ape.php');
    require_once ('Frog.php');

    
    $sheep = new Animal("shaun");
    echo "<h3>Output Akhir</h3>";
    echo "Nama Hewan = " . $sheep->name . "<br>";
    echo "Jumlah Kaki = " . $sheep->legs . "<br>"; 
    echo "Cold Blooded = " . $sheep->cold_blooded . "<br>"; 
    echo "<br>";
    
    $kodok = new Frog("buduk");
    echo "Nama Hewan = " . $kodok->name . "<br>";
    echo "Jumlah Kaki = " . $kodok->legs . "<br>"; 
    echo "Cold Blooded = " . $kodok->cold_blooded . "<br>"; 
    echo "Jump = ";
    $kodok->jump() ;
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama Hewan = " . $sungokong->name . "<br>";
    echo "Jumlah Kaki = " . $sungokong->legs . "<br>"; 
    echo "Cold Blooded = " . $sungokong->cold_blooded . "<br>"; 
    echo "Yell = ";
    $sungokong->yell() ;
    echo "<br>";


?>
